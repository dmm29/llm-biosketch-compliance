from pymongo import MongoClient
import csv

# Connect to MongoDB
client = MongoClient("mongodb://localhost:27017/")
db = client["biosketches"]
collection = db["biosketch_data"]

# Query the collection
documents = collection.find({}, {"file_name": 1, "errors": 1})

# Prepare data for CSV
data_for_csv = []
for doc in documents:
    print(doc)
    filename = doc.get("file_name", "No Filename Provided")
    errors = doc.get("errors", [])
    for error in errors:
        data_for_csv.append((filename, error))

# Write to CSV
csv_file_path = "output.csv"
with open(csv_file_path, mode='w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(["Filename", "Error"])
    for filename, error in data_for_csv:
        writer.writerow([filename, error])

