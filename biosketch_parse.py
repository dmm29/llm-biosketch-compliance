import argparse
import json
import os
import re
import sys
import time
from Bio import Entrez
from datetime import datetime
from pathlib import Path

import requests
import openai
import pdfplumber
from collections import Counter
from pymongo import MongoClient

# Argument parser setup
parser = argparse.ArgumentParser(description='Process biosketches.')
parser.add_argument('--provider', type=str, choices=['openai', 'azure'], help='Specify the AI provider to use.')
parser.add_argument('--model', type=str, choices=['gpt-3.5-turbo-0125','gpt-4-1106-preview','gpt-4-turbo', 'gpt-4o'], help='Specify the AI model to use.')
parser.add_argument('--biosketch', type=str, help='Path to biosketch')
args = parser.parse_args()

# Use the parsed arguments
biosketch_path = args.biosketch
provider = args.provider
model = args.model

# Load the evaluation tool array
with open('tools_biosketch_multi.json', 'r') as file:
    biosketch_tool = json.loads(file.read())

def set_openai_credentials(provider):
    """
    Configures OpenAI API credentials based on the specified provider.
    """
    if provider == "azure":
        openai.api_type = "azure"
        openai.api_base = os.getenv("AZURE_OPENAI_ENDPOINT")
        openai.api_key = os.getenv("AZURE_OPENAI_KEY")
        openai.api_version = "2023-12-01-preview"
    elif provider == "openai":
        openai.api_key = os.getenv("OPENAI_API_KEY")
        openai.api_type = "openai"
    else:
        raise ValueError("Unsupported provider specified.")
    return provider

set_openai_credentials(provider)

random_seed = int(time.time())


def extract_text_from_pdf(pdf_file):
    """
    Extracts text, page count, margins, most common font name, and most common font size from a PDF file.
    Also checks for the usage of DeviceRGB colors that are not associated with URLs and provides details on colored segments.
    """
    text = ''
    font_counter = Counter()
    size_counter = Counter()
    min_x1 = float('inf')
    max_x0 = float('-inf')
    colored_details = []

    # Improved regex pattern for URL detection
    url_pattern = re.compile(r'https?://[^\s/$.?#].[^\s]*', re.IGNORECASE)

    with pdfplumber.open(pdf_file) as pdf:
        all_urls = set()

        for page in pdf.pages:
            page_text = page.extract_text(x_tolerance=1, y_tolerance=3)
            if page_text:
                text += page_text
                all_urls.update(url_pattern.findall(page_text))

            current_segment = ''
            last_color = None

            for char in page.chars:
                font_counter[char['fontname']] += 1
                size_counter[char['size']] += 1
                if char['x1'] < min_x1:
                    min_x1 = char['x1']
                if char['x0'] > max_x0:
                    max_x0 = char['x0']

                if char.get('ncs') == 'DeviceRGB':
                    color = char.get('stroking_color') or char.get('non_stroking_color')
                    if color:
                        if color == last_color:
                            current_segment += char['text']
                        else:
                            if current_segment and last_color:
                                is_url = any(current_segment.strip() in url for url in all_urls)
                                colored_details.append({'segment': current_segment.strip(), 'color': last_color, 'is_url': is_url})
                            current_segment = char['text']
                            last_color = color

            # Append the last segment if any
            if current_segment and last_color:
                is_url = any(current_segment.strip() in url for url in all_urls)
                colored_details.append({'segment': current_segment.strip(), 'color': last_color, 'is_url': is_url})

        page_count = len(pdf.pages)
        most_common_font = font_counter.most_common(1)[0][0] if font_counter else None
        most_common_size = size_counter.most_common(1)[0][0] if size_counter else None

    return {
        'text': text,
        'page_count': page_count,
        'min_x1': min_x1,
        'max_x0': max_x0,
        'most_common_font': most_common_font,
        'most_common_size': most_common_size,
        'colored_details': colored_details
    }






# def extract_text_from_pdf(pdf_file):
#     """
#     Extracts text, page count, margins, most common font name, and most common font size from a PDF file.
#     """
#     text = ''
#     font_counter = Counter()
#     size_counter = Counter()
#     min_x1 = float('inf')
#     max_x0 = float('-inf')
#     uses_device_rgb = False
#
#     with pdfplumber.open(pdf_file) as pdf:
#         for page in pdf.pages:
#             text += page.extract_text(x_tolerance=1, y_tolerance=3)  # Adjust tolerances as needed
#
#             for char in page.chars:
#                 font_counter[char['fontname']] += 1
#                 size_counter[char['size']] += 1
#                 if char['x1'] < min_x1:
#                     min_x1 = char['x1']
#                 if char['x0'] > max_x0:
#                     max_x0 = char['x0']
#                 # Check if the color is RGB and not blue
#                 if char.get('ncs') == 'DeviceRGB':
#                     stroking_color = char.get('stroking_color')
#                     non_stroking_color = char.get('non_stroking_color')
#                     # Ensure the stroking color is specified and not blue
#                     if stroking_color and stroking_color != (0, 0, 1):
#                         print("DAMN: ", stroking_color)
#                         uses_device_rgb = True
#                     elif non_stroking_color and non_stroking_color != (0, 0, 1):
#                         print("DAMN: ", non_stroking_color)
#                         uses_device_rgb = True
#
#         page_count = len(pdf.pages)
#
#     most_common_font = font_counter.most_common(1)[0][0] if font_counter else None
#     most_common_size = size_counter.most_common(1)[0][0] if size_counter else None
#     return {
#         'text': text,
#         'page_count': page_count,
#         'min_x1': min_x1,
#         'max_x0': max_x0,
#         'most_common_font': most_common_font,
#         'most_common_size': most_common_size,
#         'uses_device_rgb': uses_device_rgb
#     }
#
#
#
def check_for_unacceptable_urls(text):
    """
    Checks for unacceptable URLs in the provided text.
    The only acceptable URL is to ncbi.nlm.nih.gov.
    """
    # Regular expression to find URLs
    url_pattern = re.compile(r'https?://[^\s/$.?#].[^\s]*')

    # Find all URLs in the text
    urls = url_pattern.findall(text)

    # Check for unacceptable URLs
    unacceptable_urls = [url for url in urls if "ncbi.nlm.nih.gov" not in url]

    if unacceptable_urls:
        return True, unacceptable_urls
    else:
        return False, []




def extract_biosketch(data, model, tool, random_seed):
    """
    Calls the OpenAI API to summarize data provided in text or image form.
    """
    system_prompt = "You are a detail-oriented grants administrator. Extract details from biosketches. Text should be extracted verbatim. Be as precise as possible. If you can't find a value, return an empty value ''. Only respond in JSON."
    response = openai.chat.completions.create(
        model=model,
        messages=[{"role": "system", "content": system_prompt}, {"role": "user", "content": data}],
        tools=biosketch_tool,
        tool_choice={"type": "function", "function": {"name": tool}},
        response_format={"type": "json_object"},
        temperature=0.5,
        seed=random_seed
    )
    return (response.choices[0].message.tool_calls[0].function.arguments, response.usage)


def extract_text_from_sections(text, data):
    # Define the headers for each section
    headers = {
        'personal_statement': data["personal_statement_header"],
        'positions': data["positions_appointments_honors_header"],
        'contributions': data["contributions_to_science_header"]
    }
    # Extract the positions of each header in the text
    positions = {key: text.find(header) for key, header in headers.items()}

    # Check for duplicates by finding subsequent occurrences of each header
    duplicates = {}
    for key, header in headers.items():
        # Count the occurrences of each header in the text
        count = text.count(header)
        duplicates[key] = count > 1  # True if there are duplicates, False otherwise

    # If any of the headers is not found, set its position to the length of the text
    positions = {key: (pos if pos != -1 else len(text)) for key, pos in positions.items()}
    # print(positions)

    # Extract the text for each section based on header positions
    sections = {
        'personal_statement': text[positions['personal_statement']:positions['positions']].strip(),
        'positions': text[positions['positions']:positions['contributions']].strip(),
        'contributions': text[positions['contributions']:].strip()
    }
    # print(text,positions,sections)

    # Check if any section has duplicate headers
    has_duplicates = any(duplicates.values())
    return sections, has_duplicates

# define functions/tools
section_tools = {
    'biographical': 'extract_biographical_information',
    'personal_statement': 'extract_personal_statement',
    'positions': 'extract_positions_appointments_honors',
    'contributions': 'extract_contributions_to_science'
}

# Extract text and page count from PDF
extracted_data = extract_text_from_pdf(biosketch_path)

#duke_med_pattern = r"\bDuke\s+University\s+(?:Med(?:ical)?\.?\s+)?Ctr\b|\bDuke\s+University\s+Medical\s+Center\b"
duke_med_pattern =r"\bDuke\s+(?:University\s+)?Med(?:ical)?\.?\s+Ctr\b|\bDuke\s+(?:University\s+)?Medical\s+Center\b"
    



# Search for the pattern in the text
duke_med_match = re.search(duke_med_pattern, extracted_data["text"])


# Initialize an empty dictionary to store all extracted data
combined_biosketch_data = {}

# Initialize token usage counters
total_prompt_tokens = 0
total_response_tokens = 0

biosketch_data_json, token_info = extract_biosketch(extracted_data["text"], model, "extract_biographical_details", random_seed)
biosketch_data = json.loads(biosketch_data_json)


combined_biosketch_data.update(biosketch_data)

sections, duplicate = extract_text_from_sections(extracted_data["text"], combined_biosketch_data)

# Loop through each section and call the OpenAI API
for section, text in sections.items():
    if text:  # Process only if the section text is not empty
        tool = section_tools[section]
        biosketch_data_json, token_info = extract_biosketch(text, model, tool, random_seed)
        biosketch_data = json.loads(biosketch_data_json)

        # Merge the extracted data into the combined dictionary
        combined_biosketch_data.update(biosketch_data)

        # Accumulate token usage
        total_prompt_tokens += token_info.prompt_tokens
        total_response_tokens += token_info.completion_tokens


# helper  functions
def search_pubmed_for_pmcid(first_author, title, journal, year):
    """
    Search PubMed for the PMCID of a manuscript based on the title, first author, journal, and publication year,
    using an API key for authorization.
    
    Args:
        first_author (str): The first author of the manuscript.
        title (str): The title of the manuscript.
        journal (str): The journal where the manuscript was published.
        year (int): The year of publication.
        api_key (str): API key for accessing NCBI's Entrez system.
        
    Returns:
        dict: A dictionary with the status of the search and the PMCID if available.
    """
    Entrez.email = "david.macalpine@duke.edu"
    try:
        # Build query
        query = f'({title}[Title]) AND ({first_author}[Author]) AND ({year}[PubDate])'
        
        # Perform search
        handle = Entrez.esearch(db="pubmed", term=query, retmax=10)
        result = Entrez.read(handle)
        handle.close()

        # Get list of PubMed IDs from search results
        pmid_list = result['IdList']
        if not pmid_list:
            return {"status": "Manuscript not found", "pmcid": None}

        # Fetch details for the first PubMed ID
        handle = Entrez.efetch(db="pubmed", id=pmid_list[0], retmode="xml")
        records = Entrez.read(handle)
        handle.close()

        # Attempt to extract PMCID
        article_ids = records['PubmedArticle'][0]['PubmedData']['ArticleIdList']
        for article_id in article_ids:
            if article_id.attributes.get('IdType') == 'pmc':
                pmcid = article_id.title() if hasattr(article_id, 'title') else None
                pmcid = pmcid.upper()
                return {"status": "PMCID found", "pmcid": pmcid}

        
        return {"status": "Manuscript found, no PMCID available", "pmcid": None}
        
    except Exception as e:
        print(f"Error occurred: {e}")
        return {"status": "Error in searching PubMed", "pmcid": None}




def is_similar_font(fontname, desired_fonts):
    for desired_font in desired_fonts:
        pattern = re.compile(re.escape(desired_font), re.IGNORECASE)
        if pattern.search(fontname):
            return True
    return False

def calculate_cost(model, token_info):
    total_prompt_tokens = token_info.prompt_tokens
    total_response_tokens = token_info.completion_tokens
    
    if model == "gpt-4o":
        prompt_cost_per_1k = 0.005
        response_cost_per_1k = 0.015
    elif model == "gpt-3.5-turbo-0125":
        prompt_cost_per_1k = 0.0005
        response_cost_per_1k = 0.0015
    else:
        raise ValueError("Unknown model")

    total_cost = (total_prompt_tokens / 1000 * prompt_cost_per_1k) + (total_response_tokens / 1000 * response_cost_per_1k)
    
    return total_cost

# add some items to the dictionary
contains_unacceptable_urls, found_urls = check_for_unacceptable_urls(extracted_data["text"])
if contains_unacceptable_urls:
    combined_biosketch_data["unacceptable_urls"] = found_urls
else:
    combined_biosketch_data["unacceptable_urls"] = []

# Add the page count to the combined data

path = Path(biosketch_path)
combined_biosketch_data["file_name"] = path.name
combined_biosketch_data["page_count"] = extracted_data["page_count"]
combined_biosketch_data["left_margin"] = extracted_data["min_x1"]/72
combined_biosketch_data["right_margin"] = (612-extracted_data["max_x0"])/72
combined_biosketch_data["font"] = extracted_data["most_common_font"]
combined_biosketch_data["fontsize"] = extracted_data["most_common_size"]
combined_biosketch_data["colored_details"] = extracted_data["colored_details"]
combined_biosketch_data["duplicate_header"] = duplicate
if duke_med_match:
    combined_biosketch_data["duke_medical_center"] = duke_med_match.group()
else:
    combined_biosketch_data["duke_medical_center"] = ""


# Print the total token usage
# Accumulate token usage'
total_prompt_tokens += token_info.prompt_tokens
total_response_tokens += token_info.completion_tokens
cost = calculate_cost(model, token_info)
# print(f"Total prompt tokens: {total_prompt_tokens}")
# print(f"Total response tokens: {total_response_tokens}")
# print(f"Total cost for model {model}: ${cost:.4f}")
#
combined_biosketch_data["prompt_tokens"] = total_prompt_tokens
combined_biosketch_data["response_tokens"] = total_response_tokens
combined_biosketch_data["cost"] = cost

# Print the combined data
#print(json.dumps(combined_biosketch_data, indent=2))

def regex_to_readable(regex_str):
    """
    Converts a regex pattern string into a readable string by replacing common regex syntax
    elements with typical string representations.
    """
    # Properly handle escaped characters to preserve literal meanings
    readable_str = re.sub(r'\\s\+', ' ', regex_str)  # Replace '\s+' with space
    readable_str = re.sub(r'\\.', '.', readable_str)  # Replace escaped period '\.' with '.'

    # Remove any remaining regex-specific characters that are not necessary for readability
    readable_str = re.sub(r'[\\^$*+?()\[\]{}|]', '', readable_str)
    return readable_str


def trim_string(input_string):
    """
    Trims the string to 30 characters, adding ellipsis if necessary.
    """
    return input_string[:27] + "..." if len(input_string) > 30 else input_string

def check_compliance(combined_biosketch_data):
    """
    Performs compliance checks on the combined biosketch data.
    """
    errors = []

    # Check 1: Page count should be <= 5
    if combined_biosketch_data.get("page_count", 0) > 5:
        errors.append("Error: Page count exceeds 5 pages.")
        print("Check 1: Page count (max 5) check FAILED.")
    else:
        print("Check 1: Page count (max 5) check SUCCESS.")

    # Check 2: DUMC can't be mentioned
    dumc = combined_biosketch_data.get("duke_medical_center")
    if dumc:
        errors.append(f"Warning: {dumc} should be Duke University")
        print("Check 2: DUMC check FAILED.")
    else:
        print("Check 2: DUMC check SUCCESS.")


# Check 3: warning for color
    colored_segments = combined_biosketch_data.get("colored_details", [])
    found_non_url_color = False  # Flag to check if there's any non-URL color

    for segment_info in colored_segments:
        if not segment_info['is_url']:  # Check if the segment is not part of a URL
            found_non_url_color = True
            # Append the error message with specific segment and color details
            errors.append(f"Warning: Unexpected use of color in text '{segment_info['segment']}' with color {segment_info['color']}.")

    if found_non_url_color:
        print("Check 3: Unexpected color WARNING.")
    else:
        print("Check 3: Color check SUCCESS.")



    # Check 4: margins must not be < 0.5 inches (only checking left and right)
    if combined_biosketch_data.get("left_margin") < 0.5 or combined_biosketch_data.get("right_margin") < 0.5:
        errors.append(f"Error: Left {combined_biosketch_data.get('left_margin')} or right {combined_biosketch_data.get('right_margin')} margin less than 0.5 inches")
        print("Check 4: Margin check FAILED.")
    else:
        print("Check 4: Margin check SUCCESS.")


    # Check 5: Fonts should be related to arial, georgia, helvetics, palantion linotype
    desired_fonts = ["Arial", "Georgia", "Helvetica", "Palatino Linotype"]
    font = combined_biosketch_data.get("font")
    if font and not is_similar_font(font, desired_fonts):
        errors.append(f"Warning: Font {font} is not similar to the desired fonts.")
        print("Check 5: Font check FAILED.")
    else:
        print("Check 5: Font check SUCCESS.")

    # Check 6: Fonts sizes.  
    fontsize = combined_biosketch_data.get("fontsize")
    if fontsize < 10.9: #fonts are more complicated then word with internal rounding and different size points
        errors.append(f"Error: Fontsize {fontsize}pt is smaller than 11pt")
        print("Check 6: Font size check FAILED.")
    else:
        print("Check 6: Font size check SUCCESS.")


    # Check 7: Form date should be in the future
    approved_through_str = combined_biosketch_data.get("approved_through", "")
    if approved_through_str:
        try:
            approved_through_date = datetime.strptime(approved_through_str, "%m/%d/%Y")
            if approved_through_date <= datetime.now():
                errors.append("Error: Form approved through date is obsolete.")
                print("Check 7: Form approved through date check FAILED.")
            else:
                print("Check 7: Form approved through date check SUCCESS.")
        except ValueError as e:
            errors.append("Warning: Approved through date format is incorrect.")
            print(f"Check 7: Approved through date format is incorrect. FAILED. Error: {e}")
    else:
        errors.append("Error: Approved through date is missing.")
        print("Check 7: Approved through date is missing. FAILED.")

    # Check 8: form check for extra start column.  
    education_entries = combined_biosketch_data.get("education", [])
    obsolete_start_date_found = False
    for edu in education_entries:
        # Check if 'start_date' key exists and is not empty
        if edu.get("start_date"):  # This will be True if 'start_date' is non-empty
            obsolete_start_date_found = True
            # print(f"Obsolete start date found in record: {edu}")
            break

    if obsolete_start_date_found:
        errors.append("Error: Education has obsolete start date column or date range present.")
        print("Check 8: Education form check FAILED.")
    else:
        print("Check 8: Education form check SUCCESS.")



    # Check 9: era commmons username.  
    username = combined_biosketch_data.get("era_commons_username",'')
    if not username: #fonts are more complicated then word with internal rounding and different size points
        errors.append(f"Error: ERA commons username missing")
        print("Check 9: ERA commons username check FAILED.")
    else:
        print("Check 9: ERA commons username SUCCESS.")

    #  Check 10:  Check the headers for conformation with the exprected headers
    expected_headers = {
        "personal_statement_header": r"A\.\s+Personal\s+Statement",
        "positions_appointments_honors_header": r"B\.\s+Positions,\s+Scientific\s+Appointments,\s+and\s+Honors",
        "contributions_to_science_header": r"C\.\s+Contributions\s+to\s+Science"
    }

    # Check the headers for warnings
    actual_headers = {
        "personal_statement_header": combined_biosketch_data.get("personal_statement_header", ""),
        "positions_appointments_honors_header": combined_biosketch_data.get("positions_appointments_honors_header", ""),
        "contributions_to_science_header": combined_biosketch_data.get("contributions_to_science_header", "")
    }
    if combined_biosketch_data.get("duplicate_header",""):
        errors.append(f"Error: Duplicate headers detected.")
        print(f"Check 10: Duplicate headers detected. FAIL")

    for key, pattern in expected_headers.items():
        actual = actual_headers.get(key, "")
        if not re.match(pattern, actual):
            readable_expected = regex_to_readable(pattern)
            errors.append(f"Warning: {key.replace('_', ' ')} does not match the expected header. Found: '{actual}' | Expected: '{readable_expected}'")
            print(f"Check 10: {key.replace('_', ' ')} does not match the expected header. WARNING")
        else:
            print(f"Check 10: {key.replace('_', ' ')} matches the expected header. SUCCESS")



    # Check 11: Education completion dates should be in chronological order
    education_dates = []
    for edu in combined_biosketch_data.get("education", []):
        try:
            completion_date = edu["completion_date"]
            education_date = datetime.strptime(completion_date, "%m/%Y")
            education_dates.append(education_date)
        except KeyError:
            errors.append("Error: Completion date is missing in an education record.")
            print("Check 11: Completion date is missing in an education record. FAILED.")
        except ValueError as e:
            errors.append("Error: Completion date format is incorrect in an education record.")
            print(f"Check 11: Completion date format is incorrect in an education record. FAILED. Error: {e}")

    if education_dates and education_dates != sorted(education_dates):
        errors.append("Error: Education completion dates are not in chronological order.")
        print("Check 11: Education dates chronological order check FAILED.")
    else:
        print("Check 11: Education dates chronological order check SUCCESS.")



    # Check 12: Education completion dates should be in chronological order
    education_locations = []
    for edu in combined_biosketch_data.get("education", []):
        try:
            location = edu["location"]
        except KeyError:
            errors.append("Error: Location is missing in an education record.")
            print("Check 12: Location is missing in an education record. FAILED.")
        print("Check 12: Location check SUCCESS.")

    #
    #
    #
    # check 13 ongoing projects are less than 3 years
    #
    current_date = datetime.now()

    for project in combined_biosketch_data.get("projects", []):
        try:
            end_day = int(project.get("end_date_day", 1))  # Default to the first of the month if not specified
            end_month = int(project.get("end_date_month", current_date.month))  # Default to the current month if not specified
            end_date_year = project.get("end_date_year")

            if end_date_year.isdigit() and len(end_date_year) == 4:
                end_year = int(end_date_year)
            elif end_date_year.isdigit() and len(end_date_year) == 2:
                year_int = int(end_date_year)
                if year_int < 50:
                    end_year = 2000 + year_int
                else:
                    end_year = 1900 + year_int
            else:
                raise ValueError("Year is not in a proper format.")

            project_end_date = datetime(year=end_year, month=end_month, day=end_day)

            if (current_date - project_end_date).days > 1095:  # 365 days/year * 3 years
                grant_number = project.get("grant_number", "Unknown grant number")
                errors.append(f"Error: Project with grant number {grant_number} has an end date not within the last three years.")
                print(f"Check 13: Project {grant_number} end date not within the last three years. FAILED.")

        except ValueError as e:
            errors.append(f"Warning: End date format is incorrect in project record. Error: {e}")
            print(f"Check 13: End date format is incorrect in project record. FAILED. Error: {e}")
        except Exception as e:
            errors.append("Warning: Unexpected error checking project dates.")
            print(f"Check 13: Unexpected error. FAILED. Error: {e}")

    if not errors:
        print("Check 13: All projects have end dates within the last three years. SUCCESS.")#
    #
    # #
    # #
    # #
    # #
    # #
    # current_date = datetime.now()
    # project_end_dates = []
    # for project in combined_biosketch_data.get("projects",[]):
    #     try:
    #         end_day = int(project.get("end_date_day", 1))  # Default to first of the month if not specified
    #         end_month = int(project.get("end_date_month", current_date.month))  # Default to current month if not specified
    #         end_date_year = project.get("end_date_year")
    #         # Check if the year is in the correct format (4 digits)
    #         if end_date_year.isdigit() and len(end_date_year) == 4:
    #             end_year = int(end_date_year)
    #         elif end_date_year.isdigit() and len(end_date_year) == 2:
    #             year_int = int(end_date_year)
    #             if year_int < 50:
    #                 # Years 00-29 are treated as 2000-2029
    #                 end_year = 2000 + year_int
    #             else:
    #                 # Years 30-99 are treated as 1930-1999
    #                 end_year = 1900 + year_int
    #
    #
    #
    #         project_end_date = datetime(year=end_year, month=end_month, day=end_day)
    #         project_end_dates.append(project_end_date)
    #
    #     except KeyError:
    #         errors.append(f"End date is missing in a project record.")
    #         print("Check 13: End date is missing in a project record. FAILED.")
    #     except ValueError as e:
    #         errors.append(f"End date format is incorrect in a project record. Error: {e}")
    #         print(f"Check 13: End date format is incorrect in a project record. FAILED. Error: {e}")
    #
    # # Check if all project end dates are within the last three years
    # for date in project_end_dates:
    #     if (current_date - date).days > 1095:  # 365 days/year * 3 years
    #         errors.append("Some projects have end dates not within the last three years.")
    #         print("Check 13: Projects end dates within the last three years check FAILED.")
    #         break
    # else:
    #     print("Check 13: All projects have end dates within the last three years. SUCCESS.")
    #




# Check 14 position dates and pdc
    position_dates = []
    for pos in combined_biosketch_data.get("positions", []):
        try:
            start_date_str = pos.get("start_date")
            if isinstance(start_date_str, int) and len(str(start_date_str)) == 4:
                start_date = datetime.strptime(str(start_date_str), "%Y")
                position_dates.append(start_date)
            else:
                raise ValueError("Date not in YYYY format")

            # Check for specific positions that require an end date constraint
            if pos.get("title") in ["PDC", "Private Diagnostic Clinic"]:
                end_date_str = pos.get("end_date")
                if end_date_str and isinstance(end_date_str, int) and len(str(end_date_str)) == 4:
                    end_date = datetime.strptime(str(end_date_str), "%Y")
                    if end_date.year > 2023:
                        raise ValueError("Error: End date must be no later than 2023 for positions 'PDC' or 'Private Diagnostic Clinic'")
                    else:
                        print(f"Check 14: PDC check for position {pos.get('title')} SUCCESS.")
                else:
                    raise ValueError("Error: End date missing or not in YYYY format for 'PDC' or 'Private Diagnostic Clinic'")

        except KeyError:
            errors.append("Errror: Start date is missing in a position record.")
            print("Check 14: Start date is missing in a position record. FAILED.")
        except ValueError as e:
            errors.append(f"Error: Position date format is incorrect in: {pos}. Error: {e}")
            print(f"Check 14: Position date format is incorrect in: {pos}. FAILED. Error: {e}")

    if position_dates and position_dates != sorted(position_dates, reverse=True):
        errors.append("Error: Positions dates are not in reverse chronological order.")
        print("Check 14: Positions dates reverse chronological order check FAILED.")
    else:
        print("Check 14: Positions dates reverse chronological order check SUCCESS.")



    # Check 15: Honors dates should be in reverse chronological order
    honor_dates = []
    for honor in combined_biosketch_data.get("honors", []):
        try:
            start_date_str = honor["start_date"]
            if isinstance(start_date_str, int) and len(str(start_date_str)) == 4:
                start_date = datetime.strptime(str(start_date_str), "%Y")
                honor_dates.append(start_date)
            else:
                raise ValueError("Date not in YYYY format")
        except KeyError:
            errors.append("Error: Start date is missing in an honor record.")
            print("Check 15: Start date is missing in an honor record. FAILED.")
        except ValueError as e:
            errors.append(f"Warning: Honor start date format is incorrect in: {honor}. Error: {e}")
            print(f"Check 15: Honor start date format is incorrect in: {honor}. FAILED. Error: {e}")

    if honor_dates and honor_dates != sorted(honor_dates, reverse=True):
        errors.append("Error: Honors dates are not in reverse chronological order.")
        print("Check 15: Honors dates reverse chronological order check FAILED.")
    else:
        print("Check 15: Honors dates reverse chronological order check SUCCESS.")

    # Check 16: Scientific appointments dates should be in reverse chronological order
    appointment_dates = []
    for appointment in combined_biosketch_data.get("scientific_appointments", []):
        try:
            start_date_str = appointment["start_date"]
            if isinstance(start_date_str, int) and len(str(start_date_str)) == 4:
                start_date = datetime.strptime(str(start_date_str), "%Y")
                appointment_dates.append(start_date)
            else:
                raise ValueError("Date not in YYYY format")
        except KeyError:
            errors.append("Error: Start date is missing in a scientific appointment record.")
            print("Check 16: Start date is missing in a scientific appointment record. FAILED.")
        except ValueError as e:
            errors.append(f"Warning: Scientific appointment start date format is incorrect in: {appointment}. Error: {e}")
            print(f"Check 16: Scientific appointment start date format is incorrect in: {appointment}. FAILED. Error: {e}")

    if appointment_dates and appointment_dates != sorted(appointment_dates, reverse=True):
        errors.append("Error: Scientific appointments dates are not in reverse chronological order.")
        print("Check 16: Scientific appointments dates reverse chronological order check FAILED.")
    else:
        print("Check 16: Scientific appointments dates reverse chronological order check SUCCESS.")

    # Check 17: Count of scientific contributions should be <= 5
    contributions = combined_biosketch_data.get("contributions_to_science", {}).get("list_of_contributions_to_science", [])
    if len(contributions) > 5:
        errors.append("Error: Count of scientific contributions exceeds 5.")
        print("Check 17: Scientific contributions count (max 5) check FAILED.")
    else:
        print("Check 17: Scientific contributions count (max 5) check SUCCESS.")

    # Check 18: Count of citations for personal statement should be <= 4
    personal_statement_citations = combined_biosketch_data.get("manuscripts", [])
    if len(personal_statement_citations) > 4:
        errors.append("Error: Count of citations for personal statement exceeds 4.")
        print("Check 18: Personal statement citations count (max 4) check FAILED.")
    else:
        print("Check 18: Personal statement citations count (max 4) check SUCCESS.")


    # Check 19: Count of citations for each contribution to science should be <= 4
    contributions = combined_biosketch_data.get("list_of_contributions_to_science", [])
    for i, contribution in enumerate(contributions, start=1):
        if len(contribution.get("manuscripts", [])) > 4:
            errors.append(f"Error: Count of citations for contribution {i} exceeds 4.")
            print(f"Check 19: Citations count check (max 4) for contribution {i} FAILED.")
        else:
            print(f"Check 19: Citations count check (max 4) for contribution {i} SUCCESS.")


    # Check 20: Every manuscript should have an associated PMCID if date is > 2010
    for manuscript in combined_biosketch_data.get("manuscripts", []):
        if manuscript.get("manuscript_year", 0) > 2010 and not manuscript.get("manuscript_pmcid"):
            pubmed_result = search_pubmed_for_pmcid(manuscript.get("manuscript_first_author"),manuscript.get("manuscript_title"),manuscript.get("manuscript_journal"),manuscript.get("manuscript_year"))
            pmcid = pubmed_result["pmcid"]
            if pubmed_result["status"] == "PMCID found":
                errors.append(f"Error: Manuscript '{manuscript.get('manuscript_title', '')}' in personal statement missing PMCID: {pmcid}.")
                print(f"Check 20: PMCID check for manuscript '{trim_string(manuscript.get('manuscript_title', ''))}' in personal statement FAILED.")
            elif pubmed_result["status"] == "Manuscript found, no PMCID available":
                print(f"Check 20: PMCID check for manuscript '{trim_string(manuscript.get('manuscript_title', ''))}' in personal statement SUCCESS.")
            elif pubmed_result["status"] == "Manuscript not found":
                errors.append(f"Warning:  Manuscript '{manuscript.get('manuscript_title', '')}' not found in pubmed and missing PMCID.")
                print(f"Check 20: PMCID check for manuscript '{trim_string(manuscript.get('manuscript_title', ''))}' in personal statement FAILED.")
        else:
            print(f"Check 20: PMCID check for manuscript '{trim_string(manuscript.get('manuscript_title', ''))}' in personal statement SUCCESS.")

    contributions = combined_biosketch_data.get("list_of_contributions_to_science", [])
    for i, contribution in enumerate(contributions, start=1):
        for manuscript in contribution.get("manuscripts", []):
            if manuscript.get("manuscript_year", 0) > 2010 and not manuscript.get("manuscript_pmcid"):
                pubmed_result=search_pubmed_for_pmcid(manuscript.get("manuscript_first_author"),manuscript.get("manuscript_title"),manuscript.get("manuscript_journal"),manuscript.get("manuscript_year"))
                pmcid = pubmed_result["pmcid"]
                if pubmed_result["status"] == "PMCID found":
                    errors.append(f"Error: Manuscript '{manuscript.get('manuscript_title', '')}' in contribution {i} missing PMCID: {pmcid}.")
                    print(f"Check 20: PMCID check for manuscript '{trim_string(manuscript.get('manuscript_title', ''))}' in contribution {i} FAILED.")
                elif pubmed_result["status"] == "Manuscript found, no PMCID available":
                    print(f"Check 20: PMCID check for manuscript '{trim_string(manuscript.get('manuscript_title', ''))}' in contribution{i} SUCCESS.")
                elif pubmed_result["status"] == "Manuscript not found":
                    errors.append(f"Warning: Manuscript '{manuscript.get('manuscript_title', '')}' in contribution {i} not found in pubmed and no PMCID.")
                    print(f"Check 20: PMCID check for manuscript '{trim_string(manuscript.get('manuscript_title', ''))}' in contirbution {i} FAILED.")
            else:
                print(f"Check 20: PMCID check for manuscript '{trim_string(manuscript.get('manuscript_title', ''))}' in contribution {i} SUCCESS.")



    # Check 21: Check bibliography link
    bibliography_link = combined_biosketch_data.get("bibliography_link", "")
    unacceptable_urls = combined_biosketch_data.get("unacceptable_urls", "")
    if unacceptable_urls:
        errors.append("Error: Unacceptable url(s) found.")
        print("Check 21: Unacceptable url found. FAILED.")
        
    if bibliography_link:
        try:
            response = requests.get(bibliography_link)
            if response.status_code == 200:
                print("Check 21: Bibliography link is valid and working. SUCCESS.")
            else:
                print(f"Check 21: Bibliography link returned status code {response.status_code}. FAILED.")
        except requests.exceptions.RequestException as e:
            errors.append("Error: Unable to access bibliography link.")
            print(f"Check 21: Error accessing bibliography link. FAILED. Error: {e}")
    else:
        errors.append("Warning: Bibliography link is missing.")
        print("Check 21: Bibliography link is missing. FAILED.")

    return errors

# Run compliance checks and print errors if any
errors = check_compliance(combined_biosketch_data)
combined_biosketch_data["errors"]=errors
if errors:
    print("Potential compliance errors found:")
    for error in errors:
        print(f" - {error}")
else:
    print("Biosketch data is in compliance with NIH guidelines.")

#print(json.dumps(combined_biosketch_data, indent=2))

# Uncomment below code to insert JSON data into MongoDB
# Connect to MongoDB
client = MongoClient('mongodb://localhost:27017/')
db = client['biosketches']
collection = db['biosketch_data']

# Insert JSON data into MongoDB
#result = collection.insert_one(combined_biosketch_data)
#print(f"Data inserted with record id {result.inserted_id}")
 #
