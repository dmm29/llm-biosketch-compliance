# LLMs to check biosketches for compliance

## Approach

A json schema ```tools_biosketch_merge.json``` is used to extract structured data from a biosketch -- everying from form labels to invidvidual components and citations become define objects in the json.  The LLM does all the work scraping the biosketch.  Because the returned data is structured we can iterate over the components with complicance checks:

- correct form
- training in chronological order 
- positions, honors, appointments in revere chronological order
- correct number of contributions to science
- correct number of citations per contribution
- correct number of pages
- PMCIDs for mansucripts published after 2010 (actually not sure when the
transition was)
- etc.

<details>
  <summary>JSON Schema for extract_biosketch function</summary>

```json
[
    {
        "type": "function",
        "function": {
            "name": "extract_biographical_information",
            "description": "Extracts biographical information from a biographical sketch.",
            "parameters": {
                "type": "object",
                "properties": {
                    "omb_no": {
                        "type": "array",
                        "description": "List of OMB numbers.",
                        "items": {
                            "type": "string"
                        }
                    },
                    "revision_date": {
                        "type": "string",
                        "description": "The revision date of the document."
                    },
                    "approved_through": {
                        "type": "string",
                        "description": "The approval date through which the document is valid."
                    },
                    "name": {
                        "type": "string",
                        "description": "The full name of the person."
                    },
                    "era_commons_username": {
                        "type": "string",
                        "description": "The eRA Commons username."
                    },
                    "position_title": {
                        "type": "string",
                        "description": "The position title."
                    },
                    "education": {
                        "type": "array",
                        "description": "Educational background including institution, degree, completion date, and field of study.",
                        "items": {
                            "type": "object",
                            "properties": {
                                "institution": {
                                    "type": "string",
                                    "description": "The institution where the education was received."
                                },
                                "degree": {
                                    "type": "string",
                                    "description": "The degree received."
                                },
                                "completion_date": {
                                    "type": "string",
                                    "description": "The completion date of the degree."
                                },
                                "field_of_study": {
                                    "type": "string",
                                    "description": "The field of study."
                                }
                            },
                            "required": ["institution", "degree", "completion_date", "field_of_study"]
                        }
                    },
                    "positions": {
                        "type": "array",
                        "description": "List of positions held with dates.",
                        "items": {
                            "type": "object",
                            "properties": {
                                "date": {
                                    "type": "string",
                                    "description": "The date of the position."
                                },
                                "position": {
                                    "type": "string",
                                    "description": "The position held."
                                }
                            },
                            "required": ["date", "position"]
                        }
                    },
                    "scientific_appointments": {
                        "type": "array",
                        "description": "List of scientific appointments with dates.",
                        "items": {
                            "type": "object",
                            "properties": {
                                "date": {
                                    "type": "string",
                                    "description": "The date of the appointment."
                                },
                                "appointment": {
                                    "type": "string",
                                    "description": "The scientific appointment."
                                }
                            },
                            "required": ["date", "appointment"]
                        }
                    },
                    "honors": {
                        "type": "array",
                        "description": "List of honors received with dates.",
                        "items": {
                            "type": "object",
                            "properties": {
                                "date": {
                                    "type": "string",
                                    "description": "The date of the honor."
                                },
                                "honor": {
                                    "type": "string",
                                    "description": "The honor received."
                                }
                            },
                            "required": ["date", "honor"]
                        }
                    },
                    "bibliography_link": {
                        "type": "string",
                        "description": "Link to the complete list of published work in MyBibliography."
                    }
                },
                "required": [
                    "omb_no",
                    "revision_date",
                    "approved_through",
                    "name",
                    "era_commons_username",
                    "position_title",
                    "education",
                    "positions",
                    "scientific_appointments",
                    "honors",
                    "bibliography_link"
                ]
            }
        }
    },
    {
        "type": "function",
        "function": {
            "name": "extract_personal_statement",
            "description": "Extracts the personal statement from a biographical sketch.",
            "parameters": {
                "type": "object",
                "properties": {
                    "personal_statement": {
                        "type": "object",
                        "description": "The personal statement.",
                        "properties": {
                            "statement": {
                                "type": "string",
                                "description": "The personal statement text."
                            },
                            "manuscripts": {
                                "type": "array",
                                "description": "List of manuscripts citations associated with the personal statement.",
                                "items": {
                                    "type": "object",
                                    "properties": {
                                        "manuscript_first_author": {
                                            "type": "string",
                                            "description": "The first author of the manuscript."
                                        },
                                        "manuscript_title": {
                                            "type": "string",
                                            "description": "The title of the manuscript."
                                        },
                                        "manuscript_journal": {
                                            "type": "string",
                                            "description": "The journal the manuscript was published in."
                                        },
                                        "manuscript_year": {
                                            "type": "number",
                                            "description": "The publication year of the manuscript."
                                        },
                                        "manuscript_pmcid": {
                                            "type": "string",
                                            "description": "The PubMed Central ID (PMCID) associated with the manuscript. The PMCID is a string that must be in the format PMC1234567.  "
                                        }
                                    },
                                    "required": ["manuscript_first_author", "manuscript_title", "manuscript_journal", "manuscript_year", "manuscript_pmcid"]
                                }
                            }
                        },
                        "required": ["statement", "manuscripts"]
                    }
                },
                "required": ["personal_statement"]
            }
        }
    },
    {
        "type": "function",
        "function": {
            "name": "extract_contributions_to_science",
            "description": "Extracts contributions to science from section C of a biographical sketch.",
            "parameters": {
                "type": "object",
                "properties": {
                    "contributions_to_science": {
                        "type": "array",
                        "description": "List of contributions to sciences from section C of biosketech.",
                        "items": {
                            "type": "object",
                            "properties": {
                                "contribution_summary": {
                                    "type": "string",
                                    "description": "One sentence summary of contribution to science."
                                },
                                "manuscripts": {
                                    "type": "array",
                                    "description": "List of manuscripts citations associated with contribution to science.",
                                    "items": {
                                        "type": "object",
                                        "properties": {
                                            "manuscript_first_author": {
                                                "type": "string",
                                                "description": "The first author of the manuscript."
                                            },
                                            "manuscript_title": {
                                                "type": "string",
                                                "description": "The title of the manuscript."
                                            },
                                            "manuscript_journal": {
                                                "type": "string",
                                                "description": "The journal the manuscript was published in."
                                            },
                                            "manuscript_year": {
                                                "type": "number",
                                                "description": "The publication year of the manuscript."
                                            },
                                            "manuscript_pmcid": {
                                                "type": "string",
                                                "description": "The PubMed Central ID (PMCID) associated with the manuscript. A string that must start with PMC eg. 'PMC1234567'. only include if you find the PMC string. "
                                            }
                                        },
                                        "required": ["manuscript_first_author", "manuscript_title", "manuscript_journal", "manuscript_year", "manuscript_pmcid"]
                                    }
                                }
                            },
                            "required": ["contribution_summary", "manuscripts"]
                        }
                    }
                },
                "required": ["contributions_to_science"]
            }
        }
    }
]

```
</details>


<details>
  <summary>Returned structured data from the model</summary>

```json
{
  "omb_no": [
    "0925-0001",
    "0925-0002"
  ],
  "revision_date": "10/2021",
  "approved_through": "01/31/2026",
  "name": "Micah A. Luftig",
  "era_commons_username": "MICAH.LUFTIG",
  "position_title": "Professor and Vice Chair",
  "education": [
    {
      "institution": "Louisiana State University, Louisiana",
      "degree": "B.S.",
      "completion_date": "05/1998",
      "field_of_study": "Microbiology"
    },
    {
      "institution": "Harvard University, Massachusetts",
      "degree": "Ph.D.",
      "completion_date": "09/2003",
      "field_of_study": "Virology"
    },
    {
      "institution": "Merck Research Laboratories, New Jersey",
      "degree": "Postdoctoral",
      "completion_date": "06/2007",
      "field_of_study": "Biochemistry"
    }
  ],
  "positions": [
    {
      "date": "2023-",
      "position": "Professor, Molecular Genetics & Microbiology, Duke University School of Medicine, Durham, NC"
    },
    {
      "date": "2020-",
      "position": "Scientific Advisory Board, Evrys Bio"
    },
    {
      "date": "2019 -",
      "position": "Associate Professor, Immunology, Duke University School of Medicine, Durham, NC"
    },
    {
      "date": "2017 -",
      "position": "Associate Professor, Medicine, Division of Hematological Malignancies and Cellular Therapy, Duke University School of Medicine, Durham, NC"
    },
    {
      "date": "2017 -",
      "position": "Vice Chair, Molecular Genetics & Microbiology, Duke University School of Medicine, Durham, NC"
    },
    {
      "date": "2015 -",
      "position": "Associate Professor with Tenure, Molecular Genetics & Microbiology, Duke University School of Medicine, Durham, NC"
    },
    {
      "date": "2012 - 2017",
      "position": "Assistant Professor, Medicine, Division of Hematological Malignancies and Cellular Therapy, Duke University School of Medicine"
    },
    {
      "date": "2007 - 2015",
      "position": "Assistant Professor, Molecular Genetics & Microbiology, Duke University School of Medicine, Durham, NC"
    },
    {
      "date": "2003 - 2007",
      "position": "Postdoctoral Research Fellow, Biochemistry, IRBM \"P. Angeletti\", Merck Research Laboratories, Pomezia (Rome)"
    }
  ],
  "scientific_appointments": [
    {
      "date": "2022-",
      "appointment": "Editorial Board, Viruses"
    },
    {
      "date": "2021 -",
      "appointment": "Co-Director, Cancer Biology Program, Duke Cancer Institute"
    },
    {
      "date": "2019 -",
      "appointment": "Director, Duke Viral Oncology Training Program"
    },
    {
      "date": "2018-",
      "appointment": "Director, Duke Center for Virology"
    },
    {
      "date": "2018 - 2022",
      "appointment": "Standing member, NIH VIRA study section"
    },
    {
      "date": "2015 -",
      "appointment": "Editorial Board, Journal of Virology"
    },
    {
      "date": "2014 - 2018",
      "appointment": "Director of Graduate Studies, Molecular Genetics & Microbiology Graduate Program, Duke University School of Medicine, Durham, NC"
    },
    {
      "date": "2013 -",
      "appointment": "Guest Editor, PLoS Pathogens"
    },
    {
      "date": "2011 - 2017",
      "appointment": "Leader, AIDS-Associated Opportunistic Infections and Cancer Working Group, Duke CFAR"
    },
    {
      "date": "2011 - 2014",
      "appointment": "Chair, Duke MGM Graduate Admissions Committee"
    },
    {
      "date": "2009 - 2018",
      "appointment": "Deputy Director, Duke Center for Virology"
    },
    {
      "date": "2009 - 2013",
      "appointment": "Academic Editor, PLoS One"
    },
    {
      "date": "2008 - 2019",
      "appointment": "Associate Director, Duke Viral Oncology Training Program"
    }
  ],
  "honors": [
    {
      "date": "2018",
      "honor": "Elected as a Fellow, American Association for the Advancement of Science (AAAS)"
    },
    {
      "date": "2015",
      "honor": "Faculty of 1000 Member, Virology Section"
    },
    {
      "date": "2015",
      "honor": "Priscilla Schaffer Memorial Lecture at the 40th International Herpesvirus Workshop"
    },
    {
      "date": "2012",
      "honor": "Elected Member of International EBV Association Executive Board"
    },
    {
      "date": "2011",
      "honor": "Gordon Research Conference on \u201cViruses and Cells\u201d Top Abstract Travel Award"
    },
    {
      "date": "2008",
      "honor": "Ralph Powe Junior Faculty Enhancement Award, Oak Ridge Associated Universities"
    },
    {
      "date": "2004-2006",
      "honor": "EMBO Long-Term Postdoctoral Research Fellowship, IRBM, Rome, Italy"
    }
  ],
  "bibliography_link": "http://www.ncbi.nlm.nih.gov/sites/myncbi/micah.luftig.1/bibliography/42073258/public/?sort=date&direction=ascending",
  "personal_statement": {
    "statement": "A1. Mentoring\nI am thrilled to continue to serve as a mentor for the Pharmacological Sciences Training Program. In my 15 years at Duke, I have served in several leadership and mentorship roles in graduate education. First, from 2008-2019 I served as the Associate Director of the NCI T32-supported Viral Oncology Training Program (VOTP) and currently I am the PI and Director of this program. This is a hybrid predoctoral and postdoctoral program. I also have extensive experience as a leader in graduate education at Duke having served initially as Chair of the Molecular Genetics and Microbiology (MGM) graduate admissions committee, then four years as the Director of Graduate Studies for MGM (2014-2018), and in 2017 I was appointed Vice Chair in the department with oversight of graduate students and postdocs within my purview along with faculty development. I have served as Director of the Duke Ethics Retreat in the past, been a mentor in the Duke BioCoRE program (BioCoRE is a Duke program supporting under-represented minorities in science), been a guest speaker at the BioCoRE Retreat in 2018, led several sessions at on-campus RCR events, and I have participated in numerous institution-wide strategic discussions on graduate and post-doctoral education. I also co-led an effort within MGM from 2020-2022 called the Action Group Against Racism that includes students, postdocs, staff, and faculty aimed at building and sustaining efforts to improve the overall research climate in MGM and at Duke.\nIn my own laboratory, I have trained seven Ph.D. students and five postdoctoral fellows. I currently have six Ph.D. students, two postdocs, and two technicians in the laboratory. My Ph.D. graduates have taken different paths with five initially moving on to postdoctoral fellowships, one to a research scientist position at a biotechnology company (IconOvir) and one directly to a position in clinical research management at a CRO (Nuventra). Of those that pursued postdocs, one is now Senior Director at Immunome, one is a Health Science Policy Analyst at the NIH, one is a Scientist at Merck in Oncology, one is a postdoctoral fellow at UPenn in a virology laboratory who recently accepted a faculty position at Wistar (F32 and K99/R00 recipient), and one is a Schmidt Science Fellow in a metabolism genetics lab at the University of Cambridge. My postdoctoral fellows have moved on to a number of paths as well. Three of my former postdocs are Research Track Faculty: one Assistant Professor at Northwestern University, one Associate Professor at the University of Rome Tor Vergata, and one Assistant Professor at Louisiana State University. One other postdoc is currently a Senior Scientist at Amgen, and the final one is a Scientist at LabCorps.\nA2. Scientific\nMy laboratory studies the common herpesvirus, Epstein-Barr virus (EBV), which infects over 90% of all adults worldwide. EBV establishes a latent infection in B lymphocytes and its replication is largely kept in check by a robust adaptive immune response. However, in immune-compromised individuals such as those following transplant, EBV promotes uncontrolled B-cell proliferation, which can lead to frank lymphoma. We use the model of primary human B-cell infection in vitro to study how EBV takes over the host cell transcriptome to modulate signaling pathways and control cell growth and survival. Specifically, we have discovered that early during primary B-cell infection, virus-infected cells rapidly proliferate and, as a consequence, deplete host cells of dNTPs necessary for rapid cellular DNA replication. This metabolic barrier to proliferation results in a growth-suppressive DNA damage response that suppresses outgrowth of the majority of the infected cells (1). In the course of these studies, we also discovered that the viral latent gene expression program during the first several rounds of cell division is distinct from that observed in later divisions through immortalized cell growth (2). This transition shifts from cells expressing the viral EBV nuclear antigens, or EBNAs, only (called latency IIb) to those expressing the EBNAs as well as the latent membrane proteins, or LMPs (called latency III). This heterogeneity in viral and host gene expression has implications for EBV mimicry of B-cell maturation, cell survival, and adaptive immune responses to the virus (3,4).",
    "manuscripts": [
      {
        "manuscript_first_author": "Nikitin PA",
        "manuscript_title": "An ATM/Chk2-mediated DNA damage-responsive signaling pathway suppresses Epstein-Barr virus transformation of primary human B cells.",
        "manuscript_journal": "Cell Host Microbe",
        "manuscript_year": 2010,
        "manuscript_pmcid": "PMC3049316"
      },
      {
        "manuscript_first_author": "Price AM",
        "manuscript_title": "Analysis of Epstein-Barr virus-regulated host gene expression changes through primary B-cell outgrowth reveals delayed kinetics of latent membrane protein 1-mediated NF-kappaB activation.",
        "manuscript_journal": "J Virol",
        "manuscript_year": 2012,
        "manuscript_pmcid": "PMC3457162"
      },
      {
        "manuscript_first_author": "Price AM",
        "manuscript_title": "Epstein-Barr virus ensures B cell survival by uniquely modulating apoptosis at early and late times after infection.",
        "manuscript_journal": "eLife",
        "manuscript_year": 2017,
        "manuscript_pmcid": "PMC5425254"
      },
      {
        "manuscript_first_author": "SoRelle ED",
        "manuscript_title": "Single-cell characterization of transcriptomic heterogeneity in lymphoblastoid cell lines.",
        "manuscript_journal": "eLife",
        "manuscript_year": 2021,
        "manuscript_pmcid": "PMC7867410"
      }
    ]
  },
  "contributions_to_science": [
    {
      "contribution_summary": "Identification of the DNA damage response (DDR) as a major innate tumor suppressor response limiting Epstein-Barr virus transformation of human B cells.",
      "manuscripts": [
        {
          "manuscript_first_author": "Bonglack EN",
          "manuscript_title": "Monocarboxylate transporter antagonism reveals metabolic vulnerabilities of viral-driven lymphomas.",
          "manuscript_journal": "Proc Natl Acad Sci U S A.",
          "manuscript_year": 2021,
          "manuscript_pmcid": "PMC8237662"
        },
        {
          "manuscript_first_author": "McFadden K",
          "manuscript_title": "Metabolic stress is a barrier to Epstein-Barr virus-mediated B-cell immortalization.",
          "manuscript_journal": "Proc Natl Acad Sci USA.",
          "manuscript_year": 2016,
          "manuscript_pmcid": "PMC4760815"
        },
        {
          "manuscript_first_author": "Nikitin PA",
          "manuscript_title": "An ATM/Chk2-mediated DNA damage-responsive signaling pathway suppresses Epstein-Barr virus transformation of primary human B cells.",
          "manuscript_journal": "Cell Host Microbe",
          "manuscript_year": 2010,
          "manuscript_pmcid": "PMC3049316"
        }
      ]
    },
    {
      "contribution_summary": "A new temporal model for EBV-driven B-cell latency establishment and transformation.",
      "manuscripts": [
        {
          "manuscript_first_author": "Price AM",
          "manuscript_title": "Epstein-Barr virus ensures B cell survival by uniquely modulating apoptosis at early and late times after infection.",
          "manuscript_journal": "eLife",
          "manuscript_year": 2017,
          "manuscript_pmcid": "PMC5425254"
        },
        {
          "manuscript_first_author": "Price AM",
          "manuscript_title": "To Be or Not IIb: A Multi-Step Process for Epstein-Barr Virus Latency Establishment and Consequences for B Cell Tumorigenesis.",
          "manuscript_journal": "PLoS Pathog",
          "manuscript_year": 2015,
          "manuscript_pmcid": ""
        },
        {
          "manuscript_first_author": "Price AM",
          "manuscript_title": "Analysis of Epstein-Barr virus-regulated host gene expression changes through primary B-cell outgrowth reveals delayed kinetics of latent membrane protein 1-mediated NF-kappaB activation.",
          "manuscript_journal": "J Virol",
          "manuscript_year": 2012,
          "manuscript_pmcid": ""
        }
      ]
    },
    {
      "contribution_summary": "Single cell infection biology.",
      "manuscripts": [
        {
          "manuscript_first_author": "SoRelle ED",
          "manuscript_title": "Single-cell characterization of transcriptomic heterogeneity in lymphoblastoid cell lines.",
          "manuscript_journal": "eLife",
          "manuscript_year": 2021,
          "manuscript_pmcid": ""
        },
        {
          "manuscript_first_author": "SoRelle ED",
          "manuscript_title": "Time-resolved transcriptomes reveal diverse B cell fate trajectories in the early response to Epstein-Barr virus infection.",
          "manuscript_journal": "Cell Rep.",
          "manuscript_year": 2022,
          "manuscript_pmcid": ""
        },
        {
          "manuscript_first_author": "SoRelle ED",
          "manuscript_title": "Epstein-Barr virus perpetuates B cell germinal center dynamics and generation of autoimmune-associated phenotypes in vitro.",
          "manuscript_journal": "Front Immunol.",
          "manuscript_year": 2022,
          "manuscript_pmcid": ""
        }
      ]
    },
    {
      "contribution_summary": "EBV regulation of cellular miRNAs is important for B-cell immortalization.",
      "manuscripts": [
        {
          "manuscript_first_author": "Linnstaedt SD",
          "manuscript_title": "Virally induced cellular microRNA miR-155 plays a key role in B-cell immortalization by Epstein-Barr virus.",
          "manuscript_journal": "J Virol",
          "manuscript_year": 2010,
          "manuscript_pmcid": ""
        },
        {
          "manuscript_first_author": "Forte E",
          "manuscript_title": "The Epstein-Barr virus (EBV)-induced tumor suppressor microRNA MiR-34a is growth promoting in EBV-infected B cells.",
          "manuscript_journal": "J Virol",
          "manuscript_year": 2012,
          "manuscript_pmcid": ""
        },
        {
          "manuscript_first_author": "Skalsky RL",
          "manuscript_title": "The viral and cellular microRNA targetome in lymphoblastoid cell lines.",
          "manuscript_journal": "PLoS Pathog",
          "manuscript_year": 2012,
          "manuscript_pmcid": ""
        }
      ]
    },
    {
      "contribution_summary": "Structural biology of HIV entry and neutralization",
      "manuscripts": [
        {
          "manuscript_first_author": "Luftig MA",
          "manuscript_title": "Structural basis for HIV-1 neutralization by a gp41 fusion intermediate-directed antibody.",
          "manuscript_journal": "Nat Struct Mol Biol",
          "manuscript_year": 2006,
          "manuscript_pmcid": ""
        },
        {
          "manuscript_first_author": "Montgomery DL",
          "manuscript_title": "Affinity maturation and characterization of a human monoclonal antibody against HIV-1 gp41.",
          "manuscript_journal": "mAbs",
          "manuscript_year": 2009,
          "manuscript_pmcid": ""
        }
      ]
    }
  ],
  "page_count": 5
}
```
</details>



### Example compliance checks

```python3 biosketch_parse.py --provider openai --model gpt-4o --biosketch biosketches/Luftig\ Biosketch.pdf```

Total prompt tokens: 17685

Total response tokens: 3152

Check 1: Page count  (max 5) check SUCCESS.

Check 2: Form approved through date check SUCCESS.

Check 3: Education dates chronological order check SUCCESS.

Check 4: Positions dates reverse chronological order check SUCCESS.

Check 5: Honors dates reverse chronological check SUCCESS.

Check 6: Scientific appointments dates reverse chronological order check SUCCESS.

Check 7: Scientific contributions count (max 5) check SUCCESS.

Check 8: Personal statement citations count (max 4) check SUCCESS.

Check 9: Citations count check (max 4) for contribution 1 SUCCESS.

Check 9: Citations count check (max 4) for contribution 2 SUCCESS.

Check 9: Citations count check (max 4) for contribution 3 SUCCESS.

Check 9: Citations count check (max 4) for contribution 4 SUCCESS.

Check 9: Citations count check (max 4) for contribution 5 SUCCESS.

Check 10: PMCID check for manuscript 'An ATM/Chk2-mediated DNA da...' in personal statement SUCCESS.

Check 10: PMCID check for manuscript 'Analysis of Epstein-Barr vi...' in personal statement SUCCESS.

Check 10: PMCID check for manuscript 'Epstein-Barr virus ensures ...' in personal statement SUCCESS.

Check 10: PMCID check for manuscript 'Single-cell characterizatio...' in personal statement SUCCESS.

Check 10: PMCID check for manuscript 'Monocarboxylate transporter...' in contribution 1 SUCCESS.

Check 10: PMCID check for manuscript 'Metabolic stress is a barri...' in contribution 1 SUCCESS.

Check 10: PMCID check for manuscript 'An ATM/Chk2-mediated DNA da...' in contribution 1 SUCCESS.

Check 10: PMCID check for manuscript 'Epstein-Barr virus ensures ...' in contribution 2 SUCCESS.

Check 10: PMCID check for manuscript 'To Be or Not IIb: A Multi-S...' in contribution 2 FAILED.

Check 10: PMCID check for manuscript 'Analysis of Epstein-Barr vi...' in contribution 2 FAILED.

Check 10: PMCID check for manuscript 'Single-cell characterizatio...' in contribution 3 FAILED.

Check 10: PMCID check for manuscript 'Time-resolved transcriptome...' in contribution 3 FAILED.

Check 10: PMCID check for manuscript 'Epstein-Barr virus perpetua...' in contribution 3 FAILED.

Check 10: PMCID check for manuscript 'Virally induced cellular mi...' in contribution 4 SUCCESS.

Check 10: PMCID check for manuscript 'The Epstein-Barr virus (EBV...' in contribution 4 FAILED.

Check 10: PMCID check for manuscript 'The viral and cellular micr...' in contribution 4 FAILED.

Check 10: PMCID check for manuscript 'Structural basis for HIV-1 ...' in contribution 5 SUCCESS.

Check 10: PMCID check for manuscript 'Affinity maturation and cha...' in contribution 5 SUCCESS.

Check: Bibliography link is valid. SUCCESS.

Compliance errors found:

 - Manuscript 'To Be or Not IIb: A Multi-S...' in contribution 2 does not have a PMCID.

 - Manuscript 'Analysis of Epstein-Barr vi...' in contribution 2 does not have a PMCID.

 - Manuscript 'Single-cell characterizatio...' in contribution 3 does not have a PMCID.

 - Manuscript 'Time-resolved transcriptome...' in contribution 3 does not have a PMCID.

 - Manuscript 'Epstein-Barr virus perpetua...' in contribution 3 does not have a PMCID.

 - Manuscript 'The Epstein-Barr virus (EBV...' in contribution 4 does not have a PMCID.

 - Manuscript 'The viral and cellular micr...' in contribution 4 does not have a PMCID.


## Models
- tested with openai gpt-4o
- should work on azure -- but the models are several generations behind

## Challenges

Citations can be a challenge as they are complex and dense in token space (not quite the right terminology; hard to predict given the diversity of names and abbreviations).  Also, the models thrive on structure when using json schemas.   The schema itself doesn't have to be overly specific to capture nested details for a citation like author list, title, journal, pages, pmcid -- it will find the details regardless of how the citations are formatted (eg MLA vs pubmed)   BUT if the user is mixing and matching citation styles within the scope of text being extracted it may struggle.  For example, if the first three citations are pubmed format and the fourth is MLA -- it will kind of lock in to the pubmed format and may balk or grab the wrong field from the MLA formatted citation.  

I will admit that gpt-4-1106-preview (an azure hosted model) stuggled with some of pmcids.  Which was surprising as I have used much simpler models (gpt-3.5-turbo) for citation processing without problems in the past.  I'm fairly confident that the problem is the mixing and matching of citation styles within the scope of text being extracted.  It may make sense to take a modular approach -- for each contribution section just extract each citation as a unit -- and then make several calls to simpler cheaper model to extract the indiiviuddal citation details and then merge back into the json tree.

Also, I am passing the biosketch text through the LLM three times -- once to capture biographical details, once for the personal statement, and a final time for the contributions to science.  This is in part due to the fact that the output is limited to 8K tokens and there are a lot of tokens in a biosketch.  Given that the biosketch headings are defined -- it would likely be better/cheaper to split the text into sections using regexp before feeding it to the model.  Or a modular approach to first extract major sections and then distill the sections with smaller cheaper models.

## Future
- harden compliance tests -- the model is good a returning structured data, I am
  bad at writing validation tests.
- decrease costs (test cheaper models, use batch jobs, etc)
- should work on image pdfs, but need to update the code for image handling
- check personal statement for match to activity (eg training for T32 proposal),
  perhaps suggest edits in the future.



